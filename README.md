
# weatherapi.com Laravel Package

A super simple package that returns the next 3 days forecast (more days with a paid plan) based on an IP Address. This package is based on the free API from https://www.weatherapi.com/
## Installation

First, install the package via Composer

```
composer require andyjh07/weather
```

Once that's done, run the following command:
```
php artisan weather:install
```
This will publish the config file so you can modify that directly if you need to, or simply use the `env` variables below instead.

One final thing to do at this stage is to make sure you run the migration for the results table:
```
php artisan migrate
```


## Environment Variables

To run this package correctly, you will need to add the following environment variables to your .env file

`WEATHER_API_KEY` - Your API key from weatherapi.com

`WEATHER_FORMAT` - Set to JSON by default, but XML is also available.

`WEATHER_CACHE_SECONDS` - Defaulted to 60.

`WEATHER_DAYS` - Defaulted to 3.

  
## Usage/Examples

Once you have all of your environment variables set up, you can start to display the data where you need it. If you just want to give things a quick go, you can try the below command. It uses the JSON dataset, so if you set your `WEATHER_FORMAT` to XML it won't be used here.
```
php artisan weather:get {ip}
```
Just make sure you pass an IP Address into the command as an argument:
```
php artisan weather:get 8.8.8.8
```

## Full Implementation
By now you probably want to start using this package in your application, here's how you can get it up and running super quick using the JSON implementation.

#### Update web.php route file
Open up `web.php` and modify the `/` route declaration to the following:
```
Route::get('/', function () {
    $weather = Weather::getWeatherRaw()->andReturn();

    return view('welcome', [
        'weather' => json_decode($weather)
    ]);
});
```
This will load in weather data for the default IP Address. Assuming by now you want to use your own IP Address, so just pass it in as an argument like below (I'll use 8.8.8.8 as an example):

```
$weather = Weather::getWeatherRaw("8.8.8.8")->andReturn();
```

#### Save the results (optional)
If you'd like to save your results when you call the API, you can chain the `save()` method after the initial `getWeatherRaw()` method like this:
```
$weather = Weather::getWeatherRaw("8.8.8.8")->save()->andReturn();
```
This will get the weather data, save it to the `Results` table and then return the data to the view as before.

_NB - This feature still needs some tweaking._

#### Displaying the results
The example code block further down uses Tailwind for its styling, so to get things shaping up nicely lets add the CDN file in. Open `welcome.blade.php` and add the following line in above the `</head>` line.

```
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
```

You may want to remove the inline styling between the `<style>...</style>` tags if you run into styling issues.

Next you'll want to add the block of code in that loops through the results. I placed this at the end of the second `div` which has the `max-w-6xl mx-auto` classes.

```
<h1 class="text-black text-3xl mt-8 font-bold text-center lg:text-left">{{ count($weather->forecast->forecastday)}} day forecast for {{ $weather->location->name }}, {{ $weather->location->country }}.</h1>
<div class="flex mx-auto flex-col w-2/3 lg:w-full lg:flex-row justify-center lg:justify-between mt-4 text-black">
    @foreach($weather->forecast->forecastday as $forecast)
    <div class="flex justify-center items-center lg:items-end p-4 lg:p-0 px-4">
        <div class="flex w-full items-center lg:items-end">
            <div class="flex flex-col w-1/2">
                <p class="font-bold">{{ \Carbon\Carbon::parse($forecast->date)->format('jS F Y') }}</p>
                <small>{{ $forecast->day->condition->text }}</small>
            </div>
            <div class="flex flex-col w-1/2">
                <small>Max Temp: {{ $forecast->day->maxtemp_c }}&deg;C</small>
                <small class="text-xs">Average Temp: {{ $forecast->day->avgtemp_c }}&deg;C</small>
            </div>
        </div>
        <img class="w-1/6" src="{{ $forecast->day->condition->icon }}" alt="{{ $forecast->day->condition->text }} icon">
    </div>
    @endforeach
</div>
```

You'll notice the above example is using Carbon inline to format the date returned by the API.

Hopefully, if everything has gone to plan, you should see something similar to this screenshot:

![Screenshot](https://i.ibb.co/87NTn7n/Screenshot-2021-07-13-at-23-56-34.png "Screenshot")
