<?php

return [
    'key' => env('WEATHER_API_KEY', ''),
    'format' => env('WEATHER_FORMAT', 'json'),
    'cache' => env('WEATHER_CACHE_SECONDS', 60),
    'days' => env('WEATHER_DAYS', 3),
];