<?php

namespace Andyjh07\Weather;

use Andyjh07\Weather\Models\Result;
use Illuminate\Support\Facades\{Http, Cache};


class Weather {
    private $weather;
    private $ip;

    public function getWeatherRaw($ip = "123.211.61.50")
    {
        $key = config('weather.key');
        $format = strtolower(config('weather.format'));
        $cache = (int) config('weather.cache');
        $days = (int) config('weather.days');

        $weather = Cache::remember("weather-{$ip}", $cache, function () use($format, $key, $ip, $days) {
            $weather = Http::get("//api.weatherapi.com/v1/forecast.{$format}?key={$key}&q={$ip}&days={$days}&aqi=no&alerts=no");
            return $weather->body();
        });

        $this->ip = $ip;
        $this->weather = $weather;
        return $this;
    }

    public function save()
    {
        Result::create([
            "ip" => $this->ip,
            "result" => $this->weather
        ]);

        return $this;
    }

    public function andReturn()
    {
        return $this->weather;
    }
}