<?php

namespace Andyjh07\Weather\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\{Http, Cache};

class GetWeather extends Command {
    protected $signature = 'weather:get {ip}';

    protected $description = 'Display some weather for a given IP Address';

    public function handle()
    {
        $this->info("Getting weather for IP Address: {$this->argument('ip')} - Please wait...");

        $key = config('weather.key');
        $cache = (int) config('weather.cache');
        $days = (int) config('weather.days');

        $weather = Cache::remember("weather-{$this->argument('ip')}", $cache, function () use($key, $days) {
            $weather = Http::get("//api.weatherapi.com/v1/forecast.json?key={$key}&q={$this->argument('ip')}&days={$days}&aqi=no&alerts=no");
            return $weather->body();
        });

        $results = json_decode($weather);

        foreach($results->forecast->forecastday as $forecast){
            $output = $results->location->name;
            $output .= " on ";
            $output .= \Carbon\Carbon::parse($forecast->date)->format('jS F Y');
            $output .= " will be ";
            $output .= strtolower($forecast->day->condition->text);
            $output .= ", have an average temp of ";
            $output .= $forecast->day->avgtemp_c."°C";
            $output .= " and a max temp of ";
            $output .= $forecast->day->maxtemp_c."°C.";

            $this->info($output);
        }
    }
}