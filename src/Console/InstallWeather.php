<?php

namespace Andyjh07\Weather\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallWeather extends Command {
    protected $signature = 'weather:install';

    protected $description = 'Install the WeatherAPI.com package by andyjh07';

    public function handle()
    {
        $this->info('Installing Weather...');

        $this->info('Publishing configuration...');

        if (! $this->configExists('weather.php')) {
            $this->publishConfiguration();
            $this->info('Published configuration');
        } else {
            if ($this->shouldOverwriteConfig()) {
                $this->info('Overwriting configuration file...');
                $this->publishConfiguration($force = true);
            } else {
                $this->info('Existing configuration was not overwritten');
            }
        }

        $this->info('Installed Weather!');
    }

    private function configExists($fileName)
    {
        return File::exists(config_path($fileName));
    }

    private function shouldOverwriteConfig()
    {
        return $this->confirm(
            'Config file already exists. Do you want to overwrite it?',
            false
        );
    }

    private function publishConfiguration($forcePublish = false)
    {
        $params = [
            '--provider' => "Andyjh07\Weather\WeatherServiceProvider",
            '--tag' => "config"
        ];

        if ($forcePublish === true) {
            $params['--force'] = '';
        }

       $this->call('vendor:publish', $params);
    }
}