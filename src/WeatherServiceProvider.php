<?php

namespace Andyjh07\Weather;

use Illuminate\Support\ServiceProvider;
use Andyjh07\Weather\Console\{InstallWeather, GetWeather};


class WeatherServiceProvider extends ServiceProvider
{
  public function register()
  {
    $this->app->bind('weather', function($app){
        return new Weather();
    });
    
    $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'weather');
  }

  public function boot()
  {
    if ($this->app->runningInConsole()) {
        $this->publishes([
            __DIR__.'/../config/config.php' => config_path('weather.php'),
        ], 'config');
    }

    $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

    // Register the command if we are using the application via the CLI
    if ($this->app->runningInConsole()) {
        $this->commands([
            InstallWeather::class,
            GetWeather::class
        ]);
    }
  }
}